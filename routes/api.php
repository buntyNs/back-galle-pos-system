<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register','Api\AuthController@register');
Route::post('/login','Api\AuthController@login');

// Route::post('/a','Api\UserController@index');
Route::post('/branchGet','Api\BranchController@index');
Route::post('/getAuth','Api\UserAuthoritiesController@index');
Route::post('/userGet','Api\UserController@index');
Route::post('/productGet','Api\ProductController@index');
Route::post('/grnGet','Api\GrnController@index');
Route::post('/grnLast','Api\GrnController@last');
Route::post('/supplierGet','Api\SuppllierController@index');
Route::post('/storeGet','Api\StoreController@index');
Route::post('/companyGet','Api\CompanyController@index');
Route::post('/customerGet','Api\CustomerController@index');
Route::post('/typeGet','Api\ProductTypeController@index');
Route::post('/payTypeGet','Api\PaymentTypeController@index');
Route::post('/grnSort','Api\GrnDataController@sort');
Route::post('/getLastTransferCode','Api\TransferController@getLastCode');
Route::post('/getLastInvoiceCode','Api\InvoiceController@getLastInvoiceCode');
Route::post('/invoiceGet','Api\InvoiceController@index');
Route::post('/customerLoanGet','Api\CustomerLoanController@index');
Route::post('/supplierReturnGet','Api\GrnReturnNoteController@index');
Route::post('/customerReturnGet','Api\CustomerReturnController@index');
Route::post('/supplierLoanGet','Api\SupplierLoanController@index');
Route::post('/recordGet','Api\RecordTransectionController@index');



 

Route::post('/branchEdit/{id}','Api\BranchController@update');
Route::post('/userEdit/{id}','Api\UserController@update');
Route::post('/grnEdit/{id}','Api\GrnController@update');
Route::post('/productEdit/{id}','Api\ProductController@update');
Route::post('/supplierEdit/{id}','Api\SuppllierController@update');
Route::post('/companyEdit/{id}','Api\CompanyController@update');
Route::post('/storeItemEdit/{id}','Api\StoreItemController@update');
Route::post('/storeEdit/{id}','Api\StoreController@update');
Route::post('/customerEdit/{id}','Api\CustomerController@update');


Route::post('/companyEdit/{id}','Api\CompanyController@update');
Route::post('/customerReturnEdit/{id}','Api\CustomerReturnController@update');
Route::post('/customerLoanEdit/{id}','Api\CustomerLoanController@update');
Route::post('/grnDataEdit/{id}','Api\GrnDataController@update');
Route::post('/grnReturnEdit/{id}','Api\GrnReturnNoteController@update');
Route::post('/invoiceEdit/{id}','Api\InvoiceController@update');
Route::post('/paymentTypeEdit/{id}','Api\PaymentTypeController@update');
Route::post('/productTypeEdit/{id}','Api\ProductTypeController@update');
Route::post('/storeItemEdit/{id}','Api\StoreItemController@update');
Route::post('/supplierLoanEdit/{id}','Api\SupplierLoanController@update');
Route::post('/transferEdit/{id}','Api\TransferController@update');
Route::post('/transferDetailsEdit/{id}','Api\TransferDetailsController@update');
Route::post('/invoiceDataEdit/{id}','Api\InvoiceController@updateData');














Route::post('/branchCreate','Api\BranchController@store');
Route::post('/userCreate','Api\UserController@store');
Route::post('/grnCreate','Api\GrnController@store');
Route::post('/productCreate','Api\ProductController@store');
Route::post('/supplierCreate','Api\SuppllierController@store');
Route::post('/storeCreate','Api\StoreController@store');
Route::post('/companyCreate','Api\CompanyController@store');
Route::post('/customerCreate','Api\CustomerController@store');
Route::post('/typeCreate','Api\ProductTypeController@store');
Route::post('/grnReturnNoteCreate','Api\GrnReturnNoteController@store');
Route::post('/transferNoteCreate','Api\TransferController@store');
Route::post('/invoiceCreate','Api\InvoiceController@store');
Route::post('/customerLoanCreate','Api\CustomerLoanController@store');
Route::post('/customerReturnCreate','Api\CustomerReturnController@store');
Route::post('/supplierLoanCreate','Api\SupplierLoanController@store');


Route::post('/branchGetById/{id}','Api\BranchController@show');
Route::post('/userGetById/{id}','Api\UserController@show');
Route::post('/authGetById/{id}','Api\UserAuthoritiesController@show');
Route::post('/grnGetById/{id}','Api\GrnController@show');
Route::post('/productGetById/{id}','Api\ProductController@show');
Route::post('/supplierGetById/{id}','Api\SuppllierController@show');
Route::post('/grnDataGetById/{id}','Api\GrnDataController@show');
Route::post('/getItemById/{id}','Api\StoreController@getItemById');
Route::post('/posItemById/{id}','Api\PosController@show');
Route::post('/posItemByType','Api\PosController@showByType');
Route::post('/storeGetByUserId/{id}','Api\UserAuthoritiesController@storeGetByUserId');
Route::post('/transferGetById/{id}','Api\TransferController@show');
Route::post('/transferDetailsGetById/{id}','Api\TransferDetailsController@show');
Route::post('/invoiceGetById/{id}','Api\InvoiceController@getInvoiceData');
Route::post('/invoiceGetByUser/{id}','Api\InvoiceController@invoiceGetByUser');
Route::post('/customerLoanGetById/{id}','Api\CustomerLoanController@show');
Route::post('/supplierReturnGetById/{id}','Api\GrnReturnNoteController@show');
Route::post('/itemGetByInvoice/{id}','Api\InvoiceController@itemGet');
Route::post('/supplierLoanGetById/{id}','Api\SupplierLoanController@show');


Route::post('/userDeleteById/{id}','Api\UserController@destroy');
Route::post('/productDeleteById/{id}','Api\ProductController@destroy');
Route::post('/companyDeleteById/{id}','Api\CompanyController@destroy');
Route::post('/supplierDeleteById/{id}','Api\SuppllierController@destroy');
Route::post('/branchDeleteById/{id}','Api\BranchController@destroy');
Route::post('/storeDeleteById/{id}','Api\StoreController@destroy');
Route::post('/customerDeleteById/{id}','Api\CustomerController@destroy');

Route::post('/companyDeleteById/{id}','Api\CompanyController@destroy');
Route::post('/customerReturnDeleteById/{id}','Api\CustomerReturnController@destroy');
Route::post('/customerLoanDeleteById/{id}','Api\CustomerLoanController@destroy');
Route::post('/grnDeleteById/{id}','Api\GrnController@destroy');
Route::post('/grnDataDeleteById/{id}','Api\GrnDataController@destroy');
Route::post('/grnReturnDeleteById/{id}','Api\GrnReturnNoteController@destroy');
Route::post('/invoiceDeleteById/{id}','Api\InvoiceController@destroy');
Route::post('/paymentTypeDeleteById/{id}','Api\PaymentTypeController@destroy');
Route::post('/productTypeDeleteById/{id}','Api\ProductTypeController@destroy');
Route::post('/storeItemDeleteById/{id}','Api\StoreItemController@destroy');
Route::post('/supplierLoanDeleteById/{id}','Api\SupplierLoanController@destroy');
Route::post('/transferDeleteById/{id}','Api\TransferController@destroy');
Route::post('/transferDetailsDeleteById/{id}','Api\TransferDetailsController@destroy');
Route::post('/invoiceDataDeleteById/{id}','Api\InvoiceController@destroyData');


Route::post('/getInvoiceReport','Api\InvoiceController@getReport');
Route::post('/getGrnReport','Api\GrnController@getReport');
Route::post('/getRecordReport','Api\RecordTransectionController@getRecord');

Route::post('/getGraphs','Api\ReportController@getGraphs');
Route::post('/getLoanDetails/{id}','Api\CustomerLoanController@getLoanDetails');