<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBranchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('branches', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->string('Name', 45)->nullable();
			$table->string('Address', 45)->nullable();
			$table->string('Email', 45)->nullable();
			$table->string('Contact', 45)->nullable();
			$table->integer('Mid')->unsigned()->index('fk_User_Detail1_idx');
			// $table->foreign('Mid')->references('Id')->on('user_details')->onDelete('cascade');
			$table->integer('DefaultStore_Id')->index('fk_Branch_Store1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$table->dropForeign('Mid');
		Schema::drop('branches');

	}

}
