<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBranchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('branches', function(Blueprint $table)
		{
			$table->foreign('DefaultStore_Id', 'fk_Branch_Store1')->references('Id')->on('stores')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('Mid', 'fk_User_Detail1_idx')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('branches', function(Blueprint $table)
		{
			$table->dropForeign('fk_Branch_Store1');
			$table->dropForeign('fk_User_Detail1_idx');
		});
	}

}
