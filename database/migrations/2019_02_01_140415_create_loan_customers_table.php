<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_customers', function (Blueprint $table) {
            // 'Customer_Id','Invoice_Id','Amount','Due_Date','Status'
            $table->increments('id');
            $table->integer('Customer_Id')->index('fk_Customer_idx')->unsigned();
            $table->integer('Invoice_Id')->index('fk_Invoice_idx')->unsigned();
            $table->float('Amount',10,0);
            $table->dateTime('Due_Date');
            $table->integer('Status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_customers');
    }
}
