<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_items', function (Blueprint $table) {
            // 'Store_Id','Item_Id','Selling_Price','Qty'
            $table->increments('id');
            $table->integer('Store_Id')->index('fk_Store_idx')->unsigned();
            $table->integer('Item_Id')->index('fk_Item_idx')->unsigned();
            $table->float('Selling_Price',10,0);
            $table->integer('Qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_items');
    }
}
