<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanSupplierDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_supplier_datas', function (Blueprint $table) {
            // 'Loan_Id','Payment','User_Id','Supplier_Id','Bill_Id'
            $table->increments('id');
            $table->integer('Loan_Id');
            $table->float('Payment',10,0);
            $table->integer('User_Id')->index('fk_user_idx')->unsigned();
            $table->integer('Supplier_Id')->index('fk_supplier_idx')->unsigned();
            $table->string('Bill_Id',45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_supplier_datas');
    }
}
