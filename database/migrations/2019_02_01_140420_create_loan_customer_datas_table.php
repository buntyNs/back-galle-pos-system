<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanCustomerDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_customer_datas', function (Blueprint $table) {
            // 'Loan_Id','Payment','User_Id','Customer_Id','Bill_Id'
            $table->increments('id');
            $table->integer('Loan_Id')->index('fk_Loan_idx')->unsigned();
            $table->float('Payment',10,0);
            $table->integer('User_Id')->index('fk_User_idx')->unsigned();
            $table->integer('Customer_Id')->index('fk_Customer_idx')->unsigned();
            $table->String('Bill_Id',45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_customer_datas');
    }
}
