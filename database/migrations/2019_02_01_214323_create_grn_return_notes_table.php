<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrnReturnNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grn_return_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Grn_Id')->index('fk_Grn_idx')->unsigned();
            $table->integer('User_Id')->index('fk_User_idx')->unsigned();
            $table->float('Total',10,0);
            $table->integer('Status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grn_return_notes');
    }
}
