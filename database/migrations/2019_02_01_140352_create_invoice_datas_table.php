<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_datas', function (Blueprint $table) {
            // 'Qty','Free_Qty','Discount','Selling_Price','Total_Price','Invoice_Id','Item_Id',
            $table->increments('id');
            $table->integer('Qty');
            $table->integer('Free_Qty');
            $table->float('Discount',10,0);
            $table->float('Selling_Price',10,0);
            $table->float('Total_Price',10,0);
            $table->integer('Invoice_Id')->index('fk_Invoice_Idx')->unsigned();
            $table->integer('Item_Id')->index('fk_Item_idx')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_datas');
    }
}
