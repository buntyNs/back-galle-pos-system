<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    protected $fillable = ['CName', 'Email' ,'Contact' , 'Address'];
}
