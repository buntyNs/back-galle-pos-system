<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = [
        'Name','Cid','Contact','Email','Address','Status',"IdNumber"
    ];
}
