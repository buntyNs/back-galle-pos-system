<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';
    protected $fillable = [
        'Name','Type_Id','Barcode','Units',
     ];
}
