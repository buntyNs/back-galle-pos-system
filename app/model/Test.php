<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'customers';
    protected $fillable = ['Name','Email','Contact','Address','Nic'];
}
