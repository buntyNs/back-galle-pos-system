<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
       'Discount','Total_Price','User_Id','Branch_Id','PaymentType_Id','Customer_Id','Status','Invoice_No','Payment_Data'
    ];
}
