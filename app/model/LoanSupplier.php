<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class LoanSupplier extends Model
{
    protected $fillable = [
        'Supplier_Id','Grn_Id','Amount','Due_Date','Due_Amount','Status'
      ];
}
