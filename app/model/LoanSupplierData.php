<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class LoanSupplierData extends Model
{
    protected $fillable = [
        'Loan_Id','Payment','User_Id','Supplier_Id','Bill_Id'
      ];
}
