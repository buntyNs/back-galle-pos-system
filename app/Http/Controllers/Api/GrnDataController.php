<?php

namespace App\Http\Controllers\Api;
use DB;
use Validator;
use App\model\GrnData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;

class GrnDataController extends Controller
{

    public function __construct()
    {
       
        return $this->middleware('auth:api');
    }
   
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
      
        $grnData = GrnData::join('items','items.id','grn_datas.Item_Id')
        ->join('stores','stores.id','grn_datas.Store_Id')
        ->select('grn_datas.id','items.Name as IName','grn_datas.Discount','grn_datas.Qty','grn_datas.Selling_Price','grn_datas.TotalPrice')
        ->where('Grn_Id',$id)->get();
        return json_encode( $grnData);
    }

   
    public function edit($id)
    {
        //
    }

  
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $update =  GrnData::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            return json_encode(GrnData::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }

    public function sort($id)
    {
        
    }
}
