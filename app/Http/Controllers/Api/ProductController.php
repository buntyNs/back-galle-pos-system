<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;
use App\model\Item;
use DB;
use PDOException;
use Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        return $this->middleware('auth:api');
    }


    public function index()
    {
        error_log("test");
        $item = Item::join('item_types','item_types.id','items.Type_Id')
        ->select('items.id','items.Name','item_types.Type','items.Barcode','items.Units')
        ->where('items.Status',0)
        ->get();
        return json_encode($item);
    }

   
    public function create()
    {
        
    }

   
    public function store(Request $request)
    {
            // $myModel = new Item;
            // return $myModel->getFillable();

        // 'Name','Type_Id','Barcode','Selling_Price','Qty','Status'
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type_id' =>'required',
            'units' =>'required',
            'barcode' =>'required',
   
        ]);

     
        DB::beginTransaction();
        try{
            if ($validator->fails()) {
                
                return (Validation::require());
            } 

            $Item=Item::create([
              
                'Name'=>Request('name'),
                'Type_Id'=>Request('type_id'),
                'Units'=>Request('units'),
                'Barcode'=>Request('barcode'),
               

            ]);
            DB::commit();
            return (Validation::success());
        }catch(PDOException $e){
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }


            DB::rollback();
            return(Validation::error($e));
        }
    }

  
    public function show($id)
    {
        $Item=Item::find($id);
        return json_encode($Item) ;
    }

   
    public function edit($id)
    {
      

    }

   
    public function update(Request $request, $id)
   
    {
       
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type_id' =>'required',
            'units' =>'required',
            'barcode' =>'required',

        ]);
        DB::beginTransaction();
        try {
            // echo $request;
            if ($validator->fails()) {

                return (Validation::require());
            }                                                   
            // $input = $request->all();

            $Item = Item::find($id);
            $Item->Name=Request('name');
            $Item->Type_Id=Request('type_id');
            $Item->Units=Request('units');     
            $Item->Barcode=Request('barcode');
            $Item->save();

            DB::commit();
            return (Validation::success());

        }catch(PDOException $e){
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }


            DB::rollback();
            return(Validation::error($e));
        }
    }

   
    public function destroy($id)
    {
       
        DB::beginTransaction();
        try {
            $update =  Item::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            return json_encode(Item::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
