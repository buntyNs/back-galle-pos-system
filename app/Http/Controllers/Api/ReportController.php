<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Graphs;

use App\model\Grn;
use App\model\Invoice;
use App\model\GrnReturnNote;
use App\model\CustomerReturn;

use DB;
use PDOException;
use Validator;

class ReportController extends Controller
{


    public function __construct()
    {
        return $this->middleware('auth:api');
    }
 
    public function getGraphs(Request $request)
    {
        error_log($request);
      $allGraphsData = [];
      array_push($allGraphsData,"grn",Graphs::getGrn($request->dates));
      array_push($allGraphsData,"invoice",Graphs::getInvoice($request->dates));
      array_push($allGraphsData,"grnReturn",Graphs::getReturnGrn($request->dates));
      array_push($allGraphsData,"invoiceReturn",Graphs::getReturnInvoice($request->dates));

      return $allGraphsData;
     
    }

   
}
