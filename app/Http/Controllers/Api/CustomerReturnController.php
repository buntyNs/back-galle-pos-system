<?php

namespace App\Http\Controllers\Api;
use DB;
use Validator;
use PDOException;
use App\model\Invoice;
use App\model\CustomerReturn;
use Illuminate\Http\Request;
use App\model\Branch;
use App\model\StoreItem;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;

class CustomerReturnController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:api');
    }

   
    public function index()
    {
        error_log('test');
        $customerRtn =  CustomerReturn::join('invoices','invoices.id','customer_returns.Invoice_Id')
         ->join('items','items.id','customer_returns.Item_Id')
         ->select('customer_returns.id','invoices.Invoice_No','items.Name','customer_returns.Qty',
         'customer_returns.Reason',
         'customer_returns.Refund',
         'customer_returns.created_at')
         ->where('customer_returns.Status',0)
         ->get();

        return json_encode($customerRtn);
    }

  
    public function create()
    {
       
    }

   
    public function store(Request $request)
    {
        error_log($request);
        $validator = Validator::make($request->all(), [
            'invoice' => 'required',
            'refund'=>'required',
            'reason' => 'required',
            'item' => 'required',
            'qty' => 'required',
        ]);
        DB::beginTransaction();
        try {
            if ($validator->fails()) {
                
                return (Validation::require());
            } 
           
          $id  = Invoice::where('Invoice_No',Request('invoice'))
          ->select('id','Branch_Id')
          ->get();

          error_log($id);

            $customer = CustomerReturn::create([
                'Reason' => Request('reason'),
                'Item_Id' => Request('item'),
                'Qty' => Request('qty'),
                'Refund' => Request('refund'),
                'Invoice_Id'=>$id[0]->id
            ]);
            error_log($customer);
            if($customer){
                error_log("In");
            $store_id = Branch::where('id', $id[0]->Branch_Id)
            ->select('DefaultStore_Id')
            ->get();
            $did =  $store_id[0]->DefaultStore_Id;

            error_log($did);


            $qty_old = StoreItem::where('Store_Id',$did)
            ->where('Item_Id', Request('item'))
            ->get();
            $qty = $qty_old[0]->Qty + Request('qty');
// return $qty_old;
           $update =  StoreItem::where('Store_Id',$did)
            ->where('Item_Id',Request('item'))
            ->update(['Qty' => $qty]);

            }else{
                DB::rollback();
                return (Validation::error());
            }

            // dd($customer);
         
            DB::commit();
            return(Validation::success());
        } catch (PDOException $e) {
            DB::rollback();
            return (Validation::error());
        }
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        error_log($request);
        $validator = Validator::make($request->all(), [
            'reason' => 'required',
            'refund'=>'required',
            'qty' => 'required',
        ]);
        DB::beginTransaction();
        try {
            if ($validator->fails()) {
                
                return (Validation::require());
            } 
           
            $return = CustomerReturn::find($id);
            $qtydif = Request('qty') - $return->Qty;

            $return->Reason = Request('reason');
            $return->Refund = Request('refund');
            $return->Qty = Request('qty');
            $save = $return->save();
            error_log($save);
            error_log($return);

            if($save){

                $id  = Invoice::where('id',$return->Invoice_Id)
                ->select('id','Branch_Id')
                ->get();

                error_log($id);

                $store_id = Branch::where('id', $id[0]->Branch_Id)
            ->select('DefaultStore_Id')
            ->get();
            $did =  $store_id[0]->DefaultStore_Id;
            error_log($did);

          
            error_log($qtydif);


            $qty_old = StoreItem::where('Store_Id',$did)
            ->where('Item_Id',$return->Item_Id)
            ->get();
            $qty = $qty_old[0]->Qty + $qtydif;
// return $qty_old;
           $update =  StoreItem::where('Store_Id',$did)
            ->where('Item_Id',$return->Item_Id)
            ->update(['Qty' => $qty]);

            }else{
                DB::rollback();
                return (Validation::error());
            }
         
            DB::commit();
            return(Validation::success());
        } catch (PDOException $e) {
             DB::rollback();
                return (Validation::error());
        }
    }

   
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $update =  CustomerReturn::where('id',$id)
            ->update(['Status' => 1]);
            if($update){
                $return = CustomerReturn::find($id);

                $id  = Invoice::where('id',$return->Invoice_Id)
                ->select('id','Branch_Id')
                ->get();

                $store_id = Branch::where('id', $id[0]->Branch_Id)
            ->select('DefaultStore_Id')
            ->get();
            $did =  $store_id[0]->DefaultStore_Id;

            error_log($did);


            $qty_old = StoreItem::where('Store_Id',$did)
            ->where('Item_Id', $return->Item_Id)
            ->get();
            $qty = $qty_old[0]->Qty - $return->Qty;
// return $qty_old;
           $update =  StoreItem::where('Store_Id',$did)
            ->where('Item_Id', $return->Item_Id)
            ->update(['Qty' => $qty]);


            }else{
                DB::rollback();
                return (Validation::error());
            }




            DB::commit();
            return json_encode(CustomerReturn::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return (Validation::error());
        }
    }
}
