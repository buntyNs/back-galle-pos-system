<?php

namespace App\Http\Controllers\Api;
use App\model\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Helper\Validation;
use Validator;
use DB;
use PDOException;
class CompanyController extends Controller
{
   
    public function __construct()
    {
        return $this->middleware('auth:api');
    }



    public function index()
    {
      
        return json_encode(Company::where('Status',0)->get());
    }

    
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'contact' => 'required',
        ]);
        DB::beginTransaction();
        try {
            if ($validator->fails()) {
                
                return (Validation::require());
            } 
            $company = Company::create([
                'CName' => Request('name'),
                'Address' => Request('address'),
                'Email' => Request('email'),
                'Contact' => Request('contact'),
             
            ]);
            DB::commit();
            return (Validation::success());
        } catch (PDOException $e){
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }
            DB::rollback();
            return (Validation::error($e));

        }
    }

  
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [
          
            'name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'contact' => 'required',
        ]);
        DB::beginTransaction();
        try {
          
            if ($validator->fails()) {

                return (Validation::require());
            }                                                   
            $company = Company::find($id);
            $company->CName = Request('name');
            $company->Address = Request('address');
            $company->Email = Request('email');
            $company->Contact = Request('contact');
            $company->save();

            DB::commit();
            return (Validation::success());

        }catch(Exception $e){
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }
            DB::rollback();
            return (Validation::error($e));
        }
    }

   
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $update =  Company::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            return json_encode(Company::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
