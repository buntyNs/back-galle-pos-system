<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;

class UserAuthoritiesController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:api');
    }

    public function index(Request $req )
    {
        $id=$req->all();
        $users = DB::table('user_authorities')
     
        ->join('users', 'user_authorities.user_Id', '=', 'users.id')
        ->select('user_authorities.data')
        ->where('users.id','=',$id['id'])
       
      ->get();
        return $users;

    }

    public function storeGetByUserId($id){
        $users = DB::table('user_authorities')
        ->select('Store')
        ->where('User_Id',$id)
      ->get();

        $storeString = trim($users[0]->Store, '[,]');
        $splitArray = explode(",",$storeString);
        $send = [];
        for($i = 0 ; $i < sizeof( $splitArray); $i++){
            $users = DB::table('stores')
            ->select('id','Name')
            ->where('id',$splitArray[$i])
            ->get();
            // error_log( $users[0]);
            array_push($send,$users[0]);
        }
        return  $send;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = DB::table('user_authorities')
     
        ->join('users', 'user_authorities.User_Id', '=', 'users.id')
        ->select('user_authorities.Data')
        ->where('users.id','=',$id)
        ->get();
        return json_encode($users);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
}
