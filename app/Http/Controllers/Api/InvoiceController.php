<?php

namespace App\Http\Controllers\Api;
use DB;
use Validator;
use App\model\StoreItem;
use App\model\Invoice;
use App\model\InvoiceData;
use App\model\UserDetail;
use App\model\LoanCustomer;
use App\model\Branch;
use App\model\RecordTransection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;
use App\Http\Controllers\Helper\Report;

class InvoiceController extends Controller
{
   
    public function __construct()
    {
        return $this->middleware('auth:api');
    }

public function getReport(Request $request){
    error_log($request);
    return (Report::getInvoice($request->arr));
}





    public function getLastInvoiceCode(){
        $grncode = Invoice::orderBy('created_at', 'desc')->first();
        if($grncode == ''){
            $comon = 1;
            $num_padded = sprintf("%05d", $comon);
           return 'IN'.$num_padded;
        }else{
            $gnId = $grncode->id + 1;
            $code =  sprintf("%05d", $gnId);
            return 'IN'.$code;
        }
    }

    // public function index($id)
    // {
    //     $invoice = Invoice::join('users','users.id','invoices.User_Id')
    //     ->join('customers','customers.id','invoices.Customer_Id')
    //     ->join('branches','branches.id','invoices.Branch_Id')
    //     ->join('payment_types','payment_types.id','invoices.PaymentType_Id')
    //     ->select('invoices.id','Invoice_NO','customers.Name','payment_types.Type','invoices.Total_Price','invoices.Discount')
    //     ->where('invoices.User_Id,$id')
    //     ->get();
        
    //     return json_encode($invoice);

    // }
    public function invoiceGetByUser($id)
    {
        error_log($id);
        $invoice = Invoice::join('users','users.id','invoices.User_Id')
        
        ->join('customers','customers.id','invoices.Customer_Id')
        ->join('branches','branches.id','invoices.Branch_Id')
        ->join('payment_types','payment_types.id','invoices.PaymentType_Id')
        ->select('invoices.id','Invoice_NO','customers.Name','payment_types.Type','invoices.Total_Price','invoices.Discount','invoices.created_at')
        ->where('invoices.User_Id',$id)
        ->where('invoices.Status',0)
        ->get();
        error_log($invoice);
      

        return json_encode($invoice);

    }

    public function itemGet($id){
        error_log($id);
        $data = Invoice::join('invoice_datas','invoices.id','invoice_datas.Invoice_Id')
        ->join('items','items.id','invoice_datas.Item_Id')
        ->select('items.Name','items.id')
        ->where('invoices.Invoice_No',$id)
        ->get();
        return json_encode($data);
    }


    public function getInvoiceData(Request $request,$id)
    {
     
            $data = Invoice::join('invoice_datas','invoices.id','invoice_datas.Invoice_Id')
            ->join('items','items.id','invoice_datas.Item_Id')
            ->select('invoice_datas.id','items.Name','invoice_datas.Qty','invoice_datas.Total_Price','invoice_datas.Discount','invoice_datas.Selling_Price')
            ->where('invoice_datas.Invoice_Id',$id)
            ->get();
            return json_encode($data);
       
       
    }

    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
      
        error_log($request);
       

        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'type' => 'required',
            'total' => 'required',
            'order' => 'required',
           
        ]);

        DB::beginTransaction();
        try{

            if ($validator->fails()) {

                return (Validation::require());
            }

          
            $INcode = Invoice::orderBy('created_at', 'desc')->first();
          
            $invoiceCode;
            if($INcode == ''){
               
                $comon = 1;
                $num_padded = sprintf("%05d", $comon);
                $invoiceCode = 'IN'.$num_padded;

            }else{
               
                $gnId = $INcode->id + 1;
                $code =  sprintf("%05d", $gnId);
                $invoiceCode = 'IN'.$code;
              
            }

       



//get branch and default store===============================================
            $barnch_id = UserDetail::where('User_Id',Request('user'))
            ->select('Branch_Id')
            ->get();
             $bid =  $barnch_id[0]->Branch_Id;
    
             $store_id = Branch::where('id', $bid)
            ->select('DefaultStore_Id')
            ->get();
            $did =  $store_id[0]->DefaultStore_Id;
    // =============================================================================
   



            $invoice=Invoice::create([
                'Invoice_No'=> $invoiceCode,
                'Total_Price'=>Request('total'),
                'User_Id'=>Request('user'),
                'PaymentType_Id'=>Request('type'),
                'Customer_Id'=>Request('customer'),
                'Branch_Id'=>  $bid,//add discount
                'Payment_Data' => Request('data')
              
            ]);

            if($invoice){
                error_log('this is test');
                // 'Customer_Id','Invoice_Id','Amount','Due_Date'
                if(Request('type') == 4){
                  
                    $loan=LoanCustomer::create([
                        'Customer_Id'=> Request('customer'),
                        'Invoice_Id'=>$invoice->id,
                        'Amount'=>Request('total'),
                        'Due_Amount'=>Request('total'),
                        'Due_Date'=>Request('dueDate'),
                    ]);
                    error_log('this is test2');
                }
            }
         

            if($invoice){
                $invoiceId = $invoice->id;
                $grnArray = $request->order;
                $count = count($grnArray);
              
                for($i = 0 ;$i <  $count ;$i++){
                    $json = $grnArray[$i];
                    $discount = $json['Selling_Price'] * $json['qty'] * ($json['disk']/100);
                    $total = $json['Selling_Price'] * $json['qty'] - $discount;
                    $invoice=InvoiceData::create([
                        'Invoice_Id'=>$invoiceId ,
                        'Qty'=>$json['qty'] ,
                        'Selling_Price'=> $json['Selling_Price'],
                        'Total_Price'=> $total,
                        'Item_Id'=>$json['id'] ,
                        'Discount'=>$json['disk'],
                    ]);

                    if($invoice){
                        $qty_old = StoreItem::where('Store_Id', $did)
                        ->where('Item_Id', $json ['id'])
                        ->get();

                        
                        if($qty_old[0]->Qty >= $json['qty']){
                            $qty = $qty_old[0]->Qty - $json['qty'];

                            $update =  StoreItem::where('Store_Id',$did)
                            ->where('Item_Id',$json ['id'])
                            ->update(['Qty' => $qty]);
                        

                        }else{
                            DB::rollback();
                            return (Validation::error("please check your stores quantity"));
                        }

                    }
        


                }
                // RecordTransection::create([
                //     'Code' => $invoiceId,
                //     'Type' => 'Invoice',
                //     'User' => Request('user')
                // ]);

            }


            DB::commit();
            return (Validation::success());
        }catch(Exception $e){
            DB::rollback();
            return $e;
        }

        
    }

    
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        
    }

    public function destroyData($id)
    {   
        error_log($id);
        $recode = InvoiceData::find($id);
        
    }

    public function updateData(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'price' => 'required',
            'quantity'=>'required',
            
        ]);
        DB::beginTransaction();
        try {
            if ($validator->fails()) {
                
                return (Validation::require());
            }



            error_log($request);
            error_log($id);
            $recode = InvoiceData::find($id);

            $qtydif = $recode->Qty - Request('quantity');
            $recode->Qty = Request('quantity');
            $recode->Selling_Price =Request('price');
            $recode->Total_Price = Request('quantity') * Request('price');
            $save = $recode->save();

            if($save){
                $update =  Invoice::where('id',$id)
                ->update(['Status' => 1]);
            }

    
    
            error_log($recode);
           
           
       
            DB::commit();
            return(Validation::success());
        } catch (PDOException $e) {
             DB::rollback();
                return (Validation::error());
        }
      
        
    }


   
    public function destroy(Request $request,$id)
    {
       
        DB::beginTransaction();
        try {
            $update =  Invoice::where('id',$id)
            ->update(['Status' => 1]);

            if($update){
                $getInvoice = Invoice::find($id);
               
                // remove customer loan related this invoice
                if($getInvoice->PaymentType_Id == 4){
                    $update =  LoanCustomer::where('Invoice_Id',$id)
                    ->update(['Status' => 1]);
                }
                
                $invoiceData = InvoiceData::select('Item_Id','Qty')
                ->where('Invoice_Id',$id)
                ->get();

                $count = count($invoiceData);
               

                $store_id = Branch::where('id', $getInvoice->Branch_Id)
                ->select('DefaultStore_Id')
                ->get();
 
                for($i = 0 ;$i <  $count ;$i++){
                   
                    $qty_old = StoreItem::where('Store_Id',$store_id[0]->DefaultStore_Id)
                    ->where('Item_Id',$invoiceData[$i]->Item_Id)
                    ->select('Qty')
                    ->get();

                    
                    $qty = $qty_old[0]->Qty + $invoiceData[$i]->Qty;

                   
        // return $qty_old;
                   $update =  StoreItem::where('Store_Id',$store_id[0]->DefaultStore_Id)
                    ->where('Item_Id',$invoiceData[$i]->Item_Id)
                    ->update(['Qty' => $qty]);
                 
                

                }
                
                


            }else{
                DB::rollback();
                return (Validation::error("please check your stores quantity"));
            }
            DB::commit();
            
            $invoice = Invoice::join('users','users.id','invoices.User_Id')
            ->join('customers','customers.id','invoices.Customer_Id')
            ->join('branches','branches.id','invoices.Branch_Id')
            ->join('payment_types','payment_types.id','invoices.PaymentType_Id')
            ->select('invoices.id','Invoice_NO','customers.Name','payment_types.Type','invoices.Total_Price','invoices.Discount','invoices.created_at')
            ->where('invoices.User_Id',$request->id)
            ->where('invoices.Status',0)
            ->get();
        
        return json_encode($invoice);
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
