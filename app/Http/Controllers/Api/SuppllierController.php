<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;
use DB;
use App\model\Supplier;
use Validator;
use PDOException;

class SuppllierController extends Controller
{
    public $successStatus = 200;
   

    public function __construct()
    {
        return $this->middleware('auth:api');
    }


    public function index()
    {
        // 'suppliers.id','suppliers.Name','suppliers.Email','suppliers.Contact','suppliers.Address',suppliers.IdNumber,companies.Name'


        // return json_encode(Supplier::where('Status',0)->get());

        $users = DB::table('companies')
        ->join('suppliers', 'companies.id','suppliers.Cid')
        ->select('suppliers.id','suppliers.Name','suppliers.Contact','suppliers.Email','suppliers.Address','companies.CName')
        ->where('suppliers.Status',0)
        ->get();
      return json_encode($users) ;


    //     $supplier = DB::table('companies')
    //     ->join('suppliers', 'companies.id', '=', 'suppliers.Cid')
    //     ->select('*')
    //     ->where('Status',0)
    //     ->get();
    //   return json_encode($supplier) ;
    }

    
    public function create()
    {
      
    }
    // 'Name','Cid','Contact','Email','Address','Status',"IdNumber"
    public function store(Request $request)
    { 
        error_log('test');
        error_log($request);
        $validator = Validator::make($request->all(), [
          
            'Name' => 'required',
            'Contact' => 'required',
            'Email' => 'required',
            'Address' => 'required',
            'Cid' => 'required',
            'NIC' => 'required'

        ]);

        DB::beginTransaction();
        try{
            if ($validator->fails()) {
                
                return (Validation::require());
            }

            $supplier=Supplier::create([
                'Name'=>Request('Name'),
                'Contact'=>Request('Contact'),
                'Email'=>Request('Email'),
                'Address'=>Request('Address'),
                'Cid'=>Request('Cid'),
                'IdNumber'=>Request('NIC'),

            ]);
            DB::commit();
            return (Validation::success());
        }catch(PDOException $e){
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }
            DB::rollback();
            return (Validation::error($e));
        }
    }

   
    public function show($id)
    {
    $supplier=Supplier::find($id);
    return json_encode($supplier) ;
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'Name' => 'required',
            'Contact' => 'required',
            'Email' => 'required',
            'Address' => 'required',
            'Cid' => 'required',
            'NIC' => 'required'


        ]);
        DB::beginTransaction();
        try {
            // echo $request;
            if ($validator->fails()) {
                
                return (Validation::require());
            }                                                 
            $input = $request->all();

            $Supplier = Supplier::find($id);
            $Supplier->Name=Request('Name');
            $Supplier->Contact=Request('Contact');
            $Supplier->Email=Request('Email');      
            $Supplier->Address=Request('Address');     
            $Supplier->Cid=Request('Cid');
            $Supplier->IdNumber=Request('NIC');
            $Supplier->save();

            DB::commit();
            return (Validation::success());
        }catch(\Exception $e){

            DB::rollback();
            return $e;
        }
    }

   
    public function destroy($id)
    {
       
        DB::beginTransaction();
        try {
            $update =  Supplier::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            $users = DB::table('companies')
            ->join('suppliers', 'companies.id','suppliers.Cid')
            ->select('suppliers.id','suppliers.Name','suppliers.Contact','suppliers.Email','suppliers.Address','companies.CName')
            ->where('suppliers.Status',0)
            ->get();
          return json_encode($users) ;
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
