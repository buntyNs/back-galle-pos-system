<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;
use App\model\Customer;
use App\model\CustomerReturn;
use DB;
use Validator;
use PDOException;
class CustomerController extends Controller
{
    // ['Name', 'Email' ,'Contact' , 'Address','Id_Number','Status']

    public function __construct()
    {
       
        return $this->middleware('auth:api');
    }

    public function index()
    {
    
        return json_encode(Customer::where('Status',0)->get());
    }

   
    public function create()
    {
        
    }

    
    public function store(Request $request)
    {   
        // error_log($request);
        // $myModel = new CustomerI;
        // return $myModel->getFillable();
        
        // dd($obj->fillable);
        
        // error_log($obj->fillable);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'contact' => 'required',
            'address' => 'required',
            'nic' => 'required',
        ]);
        DB::beginTransaction();
        try {
            if ($validator->fails()) {
                return(Validation::require());
            } 
           
            // $myModel = new Test;
            // return $myModel->getFillable();
            
            $customer = Customer::create([
                'Name' => Request('name'),
                'Address' => Request('address'),
                'Email' => Request('email'),
                'Contact' => Request('contact'),
                'Nic'=>Request('nic')
            ]);

            // dd($customer);
         
            DB::commit();
            return( Validation::success());
        } catch (PDOException $e) {
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }


            DB::rollback();
            return(Validation::error($e));
        }
    }

    
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'contact' => 'required',
            'address' => 'required',
            'nic' => 'required',
        ]);

        DB::beginTransaction();
        try {
            // echo $request;
            if ($validator->fails()) {

                return(Validation::require());
            }                                                   
          
            $customer = Customer::find($id);
            $customer->Name=Request('name');
            $customer->Contact=Request('contact');
            $customer->Email=Request('email');      
            $customer->Address=Request('address');     
            $customer->Nic=Request('nic');
            $customer->save();

            DB::commit();
            return( Validation::success());
        }catch(PDOException $e){

            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }


            DB::rollback();
            return(Validation::error($e));
        }

    }

   
    public function destroy($id)
    {
        
      
        DB::beginTransaction();
        try {
            $update =  Customer::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            return json_encode(Customer::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
