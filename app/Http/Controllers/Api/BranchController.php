<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\model\Branch;
use App\Http\Controllers\Helper\Validation;
use Illuminate\Http\Request; 
use Validator;
use DB;
use PDOException;
class BranchController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:api');
    }

    public function index()
    {
       
        return json_encode(Branch::where('Status',0)->get());

    }

    public function create(Request $request)
    {

       
    }

    public function store(Request $request)
    {
        error_log($request);
        $validator = Validator::make($request->all(), [
            'branch_name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'manager' => 'required',
            'contact' => 'required',
            'DefaultStore_Id' => 'required',

        ]);
      
        DB::beginTransaction();
        try {
            if ($validator->fails()) {
                
                return (Validation::require());
            } 

            Branch::create([
                'Name' => Request('branch_name'),
                'Address' => Request('address'),
                'Email' => Request('email'),
                'Mid'=>Request('manager'),
                'Contact' => Request('contact'),
                'DefaultStore_Id' => Request('DefaultStore_Id'),
            ]);
          
            DB::commit();
            return (Validation::success());
        } catch (PDOException $e){
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }
            DB::rollback();
            return (Validation::error($e));

        }
    }

    public function show($id)
    {
        $Branch = Branch::find($id);
        return json_encode( $Branch);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        error_log($request);
        error_log($id);
        $rules = array(
            'name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'manager' => 'required',
            'contact' => 'required',
            'DefaultStore_id' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        // // process the login
        if ($validator->fails()) {
            return (Validation::require());
        } else {
            // store
     
        try{
            error_log("test1");
            $Branch = Branch::find($id);
            error_log($Branch);
            $Branch->Name= Request('name');
            $Branch->Address= Request('address');
            $Branch->Email = Request('email');
            $Branch->Mid = Request('manager');
            $Branch->Contact = Request('contact');     
            $Branch->DefaultStore_Id = Request('DefaultStore_id');
            $Branch->save();

            error_log("test");
            return (Validation::success());

        }catch(PDOException $e){
            error_log($e);
          
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }
            DB::rollback();
            return (Validation::error($e));
        }
            
      
        }
    }

    public function destroy($id)
    {
      
        DB::beginTransaction();
        try {
            $update =  Branch::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            return json_encode(Branch::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
