<?php

namespace App\Http\Controllers\Api;
use Validator;
use DB;
use App\model\TransferDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;

class TransferDetailsController extends Controller
{
    
    public function __construct()
    {
        return $this->middleware('auth:api');
        
    }

    public function index()
    {
        //
    }

  
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        $data = TransferDetail::where('Transfer_Id',$id)
        ->select('id','Name','Qty')
        ->get();
        return json_encode( $data);
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $update =  TransferDetail::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            return json_encode(TransferDetail::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
