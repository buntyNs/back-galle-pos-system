<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\RecordTransection;
use App\Http\Controllers\Helper\Validation;
use App\Http\Controllers\Helper\Report;
use Validator;
use DB;
use PDOException;

class RecordTransectionController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api');
    }



    public function index()
    {
      
        return json_encode(RecordTransection::where('Status',0)->get());
    }


    public function getRecord(Request $request){
        error_log($request);
        return (Report::getRecord($request->arr));
    }
}
