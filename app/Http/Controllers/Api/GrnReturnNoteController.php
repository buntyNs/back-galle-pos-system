<?php

namespace App\Http\Controllers\Api;
use DB;
use Validator;
use App\model\GrnReturnNote;
use App\model\StoreItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;

class GrnReturnNoteController extends Controller
{
  
    public function __construct()
    {
        return $this->middleware('auth:api');
    }


    public function index()
    {
         $grnNote =  GrnReturnNote::join('grns','grns.id','grn_return_notes.Grn_Id')
         ->join('users','users.id','grn_return_notes.User_Id')
         ->join('suppliers','suppliers.id','grn_return_notes.Supplier_Id')
         ->join('items','items.id','grn_return_notes.Item_Id')
         ->join('stores','stores.id','grn_return_notes.Store_Id')
         ->select('grn_return_notes.id','grns.Grn_No','users.name as user'
         ,'suppliers.Name as supplierName','items.Name as ItemName','stores.Name',
         'grn_return_notes.Qty','grn_return_notes.Reason',
         'grn_return_notes.Refound')
         ->where('grn_return_notes.Status',0)
         ->get();

         return json_encode($grnNote);


         

    }

  
    public function create()
    {
        //
    }

  
    public function store(Request $request)
    {
       
        // ['Grn_Id', 'User_Id' ,'Supplier_Id','Item_Id','Store_Id','Qty','Refound','Reason','Status'];

        $validator = Validator::make($request->all(), [
            'grn' => 'required',
            'supplier' => 'required',
            'item' => 'required',
            'store' => 'required',
            'qty' => 'required',
            'refund' => 'required',
            'reason' => 'required',
            
        ]);

        DB::beginTransaction();
        
        try{
            if ($validator->fails()) {

                return(Validation::require());
            }


            $grnNote=GrnReturnNote::create([
              
                'Grn_Id'=>Request('grn'),
                'User_Id'=> 1,//need to modifi 
                'Supplier_Id'=>Request('supplier'),
                'Item_Id'=>Request('item'),
                'Store_Id'=>Request('store'),
                'Qty'=>Request('qty'),
                'Refound'=>Request('refund'),
                'Reason'=>Request('reason'),
              
            ]);

         
            
            if($grnNote){
               
                $qty_old = StoreItem::where('Store_Id',Request('store'))
                            ->where('Item_Id', Request('item'))
                            ->get();
                            $qty = $qty_old[0]->Qty - Request('qty');
                // return $qty_old;
                           $update =  StoreItem::where('Store_Id',Request('store'))
                            ->where('Item_Id',Request('item'))
                            ->update(['Qty' => $qty]);
                          
            }else{
                DB::rollback();
                return (Validation::error());
            }

            DB::commit();
            return (Validation::success());
        }catch(Exception $e){
            DB::rollback();
            return (Validation::error());
        }


    }

    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        error_log($request);

        $validator = Validator::make($request->all(), [

            'qty' => 'required',
            'refund' => 'required',
            'reason' => 'required',
            
        ]);

        DB::beginTransaction();
        
        try{
            if ($validator->fails()) {

                return(Validation::require());
            }
            $note = GrnReturnNote::find($id);

            $qtydif = $note->qty  - Request('qty');

            $note->Reason = Request('reason');
            $note->Refound = Request('refund');
            $note->Qty = Request('qty');
            $save = $note->save();
            
            if($save){
                $note = GrnReturnNote::find($id);
                $qty_old = StoreItem::where('Store_Id',$note->Store_Id)
                ->where('Item_Id',$note->Item_Id)
                ->get();

                $qty = $qty_old[0]->Qty + $qtydif;

                $store = StoreItem::where('Store_Id',$note->Store_Id)
                ->where('Item_Id',$note->Item_Id)
                ->update(['Qty'=>$qty]);


            }else{
            DB::rollback();
            return (Validation::error());

            }
            DB::commit();
            return(Validation::success());

        
    }catch (Exception $e) {
        DB::rollback();
        return (Validation::error());
    }
}

  
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $update =  GrnReturnNote::where('id',$id)
            ->update(['Status' => 1]);

            if($update){
                $note = GrnReturnNote::find($id);
                $qty_old = StoreItem::where('Store_Id',$note->Store_Id)
                ->where('Item_Id',$note->Item_Id)
                ->get();

                $qty = $qty_old[0]->Qty + $note->Qty;

                $store = StoreItem::where('Store_Id',$note->Store_Id)
                ->where('Item_Id',$note->Item_Id)
                ->update(['Qty'=>$qty]);

               
               
            }else{
                DB::rollback();
                return(Validation::error());

            }
            DB::commit();
            $grnNote =  GrnReturnNote::join('grns','grns.id','grn_return_notes.Grn_Id')
            ->join('users','users.id','grn_return_notes.User_Id')
            ->join('suppliers','suppliers.id','grn_return_notes.Supplier_Id')
            ->join('items','items.id','grn_return_notes.Item_Id')
            ->join('stores','stores.id','grn_return_notes.Store_Id')
            ->select('grn_return_notes.id','grns.Grn_No','users.name'
            ,'suppliers.Name','items.Name as ItemName','stores.Name',
            'grn_return_notes.Qty','grn_return_notes.Reason',
            'grn_return_notes.Refound')
            ->where('grn_return_notes.Status',0)
            ->get();
   
            return json_encode($grnNote);



         
        } catch (Exception $e) {
            DB::rollback();
            return(Validation::error());
        }
    }
}
