<?php

namespace App\Http\Controllers\Api;
use DB;
use Validator;
use App\model\LoanSupplier;
use App\model\LoanSupplierData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;

class SupplierLoanController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api');
    }

    
    public function index()
    {
        // error_log('inside');
        // $loan = LoanSupplier::join('suppliers.id','loan_suppliers.Supplier_Id')
        // ->join('grns','grns.id','loan_suppliers.Grn_Id')
        // ->select('*')
        // ->where('loan_suppliers.Status',0)
        // ->get();
        // return json_encode($loan);

        $loan = LoanSupplier::join('suppliers','suppliers.id','loan_suppliers.Supplier_Id')
        ->join('grns','grns.id','loan_suppliers.Grn_Id')
        ->select(
            'loan_suppliers.id','suppliers.Name','grns.Grn_No','loan_suppliers.Amount',
            'loan_suppliers.Due_Amount','loan_suppliers.Due_Date','loan_suppliers.created_at')
        ->where('loan_suppliers.Status',0)->get();
        return json_encode( $loan);

        //  'loan_suppliers.id','suppliers.Name','grns_Grn_No','loan_suppliers.Amount',
        //  'loan_suppliers.Due_Amount','loan_suppliers.Due_Date','loan_suppliers.created_at'
    }

  
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        error_log($request);
        $validator = Validator::make($request->all(), [
            'loan_id' => 'required',
            'payment' => 'required',
            'user_id' => 'required',
          
           
        ]);
        DB::beginTransaction();
        try {
            if ($validator->fails()) {
                
                return (Validation::require());
            } 
           
            // $myModel = new Test;
            // return $myModel->getFillable();
            
            $LOcode = LoanSupplierData::orderBy('created_at', 'desc')->first();
          
            $loanCode;
            if($LOcode == ''){
               
                $comon = 1;
                $num_padded = sprintf("%05d", $comon);
                $loanCode = 'LO'.$num_padded;

            }else{
               
                $gnId = $LOcode->id + 1;
                $code =  sprintf("%05d", $gnId);
                $loanCode = 'LO'.$code;
              
            }






            $customer = LoanSupplierData::create([
                'Loan_Id' => Request('loan_id'),
                'Payment' => Request('payment'),
                'User_Id' => Request('user_id'),
                'Bill_Id'=> $loanCode
            ]);

            if($customer){
                $amount_old = LoanSupplier::where('id',Request('loan_id'))
                ->select('Due_Amount')
                ->get();

                $amount_new = $amount_old[0]->Due_Amount -  Request('payment');

                $amount_old = LoanSupplier::where('id',Request('loan_id'))
                ->update(['Due_Amount' => $amount_new]);
                
            }else{
                DB::rollback();
            }

            // dd($customer);
         
            DB::commit();
            return (Validation::success());
        } catch (PDOException $e){
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }
            DB::rollback();
            return (Validation::error($e));

        }
    }

   
    public function show($id)
    {
        error_log($id);
        $loan = LoanSupplierData::join('loan_suppliers','loan_suppliers.id','loan_supplier_datas.Loan_Id')
        ->join('users','users.id','loan_supplier_datas.User_Id')
        ->select('loan_supplier_datas.id','loan_supplier_datas.Payment','users.name','loan_supplier_datas.Bill_Id','loan_supplier_datas.created_at')
        ->where('loan_supplier_datas.Loan_Id',$id)->get();
        return json_encode( $loan);
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $update =  LoanSupplier::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            return json_encode(LoanSupplier::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
