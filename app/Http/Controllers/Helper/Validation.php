<?php

namespace App\Http\Controllers\Helper;


class Validation
{
    public static function require()
    {
        $data = [
            "code" => 402,
            "resend"=>true
        ];
        return(json_encode($data));
    }

    public static function success()
    {
        $data = [
            "code" => 200,
            "resend"=>false
        ];
        return(json_encode($data));
    }

    public static function duplicate($value)
    {
        $data = [
            "code" => 600,
            "value" =>$value,
            "resend"=>true
        ];
        return(json_encode($data));
    }

    public static function error($value)
    {
        $data = [
            "code" => 800,
            "value" =>$value,
            "resend"=>true
        ];
        return(json_encode($data));
    }

    
    
}
