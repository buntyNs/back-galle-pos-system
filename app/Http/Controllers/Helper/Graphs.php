<?php

namespace App\Http\Controllers\Helper;
use App\model\Grn;
use App\model\Invoice;
use App\model\GrnReturnNote;
use App\model\CustomerReturn;


class Graphs
{
    public static function getGrn($date)
    {   
        $pointArray = [];
        $count = count($date); 
        error_log($count);
        for($i = 0 ; $i < $count - 1 ; $i++){

         $total =  Grn::where('grns.Status',0)
         ->whereBetween('grns.created_at',array($date[$i],$date[($i + 1)]))
         ->sum('Grn_Total');
         error_log($total);
         array_push($pointArray,$total);
        }
 
       
        return($pointArray);
    }

    public static function getInvoice($date)
    {
        $pointArray = [];
        $count = count($date); 
        error_log($count);
        for($i = 0 ; $i < $count - 1 ; $i++){

         $total =  Invoice::where('invoices.Status',0)
         ->whereBetween('invoices.created_at',array($date[$i],$date[($i + 1)]))
         ->sum('Total_Price');
         error_log($total);
         array_push($pointArray,$total);
        }
 
       
        return($pointArray);
    }

    public static function getReturnGrn($date)
    {
        $pointArray = [];
        $count = count($date); 
        error_log($count);
        for($i = 0 ; $i < $count - 1 ; $i++){

         $total =  GrnReturnNote::where('grn_return_notes.Status',0)
         ->whereBetween('grn_return_notes.created_at',array($date[$i],$date[($i + 1)]))
         ->sum('Qty');
         error_log($total);
         array_push($pointArray,$total);
        }
 
       
        return($pointArray);
    }

    public static function getReturnInvoice($date)
    {
        $pointArray = [];
        $count = count($date); 
        error_log($count);
        for($i = 0 ; $i < $count - 1 ; $i++){

         $total =  CustomerReturn::where('customer_returns.Status',0)
         ->whereBetween('customer_returns.created_at',array($date[$i],$date[($i + 1)]))
         ->sum('Qty');
         error_log($total);
         array_push($pointArray,$total);
        }
 
       
        return($pointArray);
    }

    
    
}
