<?php

namespace App\Http\Controllers\Helper;
use App\model\Invoice;
use App\model\Grn;
use App\model\RecordTransection;


class Report
{
    public static function getInvoice($data)
    {
        error_log($data[0][0]);
        // error_log();
        $count = count($data) - 1;
        error_log($data[$count-2][1]);
        error_log($data[$count-1][1]);

        $invoice;
        switch($data[$count] - 2){
            case 0 : error_log('invoice case 01'); 
             $invoice = Invoice::join('users','users.id','invoices.User_Id')
            ->join('customers','customers.id','invoices.Customer_Id')
            ->join('branches','branches.id','invoices.Branch_Id')
            ->join('payment_types','payment_types.id','invoices.PaymentType_Id')
            ->select('invoices.id','Invoice_NO','customers.Name','payment_types.Type','invoices.Total_Price','invoices.Discount','invoices.created_at')
            ->where('invoices.Status',0)
            ->whereBetween('invoices.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->get();
            break;
            case 1 :
              $invoice = Invoice::join('users','users.id','invoices.User_Id')
            ->join('customers','customers.id','invoices.Customer_Id')
            ->join('branches','branches.id','invoices.Branch_Id')
            ->join('payment_types','payment_types.id','invoices.PaymentType_Id')
            ->select('invoices.id','Invoice_NO','customers.Name','payment_types.Type','invoices.Total_Price','invoices.Discount','invoices.created_at')
            ->where($data[0][0],$data[0][1])
            ->whereBetween('invoices.created_at',array($data[$count-2][1],$data[$count-1][1]))
            ->where('invoices.Status',0)
            ->get();
            break;

            case 2 : $invoice = Invoice::join('users','users.id','invoices.User_Id')
            ->join('customers','customers.id','invoices.Customer_Id')
            ->join('branches','branches.id','invoices.Branch_Id')
            ->join('payment_types','payment_types.id','invoices.PaymentType_Id')
            ->select('invoices.id','Invoice_NO','customers.Name','payment_types.Type','invoices.Total_Price','invoices.Discount','invoices.created_at')
            ->where($data[0][0],$data[0][1])
            ->where($data[1][0],$data[1][1])
            ->whereBetween('invoices.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('invoices.Status',0)
            ->get();
            break;

            case 3 : $invoice = Invoice::join('users','users.id','invoices.User_Id')
            ->join('customers','customers.id','invoices.Customer_Id')
            ->join('branches','branches.id','invoices.Branch_Id')
            ->join('payment_types','payment_types.id','invoices.PaymentType_Id')
            ->select('invoices.id','Invoice_NO','customers.Name','payment_types.Type','invoices.Total_Price','invoices.Discount','invoices.created_at')
            ->where($data[0][0],$data[0][1])
            ->where($data[1][0],$data[1][1])
            ->where($data[2][0],$data[2][1])
            ->whereBetween('invoices.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('invoices.Status',0)
            ->get();
            break;

            case 4 : $invoice = Invoice::join('users','users.id','invoices.User_Id')
            ->join('customers','customers.id','invoices.Customer_Id')
            ->join('branches','branches.id','invoices.Branch_Id')
            ->join('payment_types','payment_types.id','invoices.PaymentType_Id')
            ->select('invoices.id','Invoice_NO','customers.Name','payment_types.Type','invoices.Total_Price','invoices.Discount','invoices.created_at')
            ->where($data[0][0],$data[0][1])
            ->where($data[1][0],$data[1][1])
            ->where($data[2][0],$data[2][1])
            ->where($data[3][0],$data[3][1])
            ->whereBetween('invoices.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('invoices.Status',0)
            ->get();
            break;



        }
       
      

        return json_encode($invoice);
    }

    public static function getGrn($data)
    {
        $count = count($data) - 1;
        $grnGet;
        switch($data[$count]-2){
            case 0: $grnGet = Grn::join('suppliers','suppliers.id','grns.Supplier_Id')
            ->join('stores','stores.id','grns.Stock_Id')
            ->join('users','users.id','grns.User_Id')
            ->join('payment_types','payment_types.id','grns.Payment_Type_Id')
            ->select('grns.id','suppliers.Name as SName','grns.Grn_No'
            ,'grns.InvoiceId','grns.Grn_Total',
            'grns.Discount','stores.Name','users.name',
            'payment_types.Type')
            ->whereBetween('invoices.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('grns.Status',0)
            ->get();
            break;
            case 1: error_log("case 1"); 
            $grnGet = Grn::join('suppliers','suppliers.id','grns.Supplier_Id')
            ->join('stores','stores.id','grns.Stock_Id')
            ->join('users','users.id','grns.User_Id')
            ->join('payment_types','payment_types.id','grns.Payment_Type_Id')
            ->select('grns.id','suppliers.Name as SName','grns.Grn_No'
            ,'grns.InvoiceId','grns.Grn_Total',
            'grns.Discount','stores.Name','users.name',
            'payment_types.Type')
            ->where($data[0][0],$data[0][1])
            ->whereBetween('invoices.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('grns.Status',0)
            ->get();
            break;

            case 2: $grnGet = Grn::join('suppliers','suppliers.id','grns.Supplier_Id')
            ->join('stores','stores.id','grns.Stock_Id')
            ->join('users','users.id','grns.User_Id')
            ->join('payment_types','payment_types.id','grns.Payment_Type_Id')
            ->select('grns.id','suppliers.Name as SName','grns.Grn_No'
            ,'grns.InvoiceId','grns.Grn_Total',
            'grns.Discount','stores.Name','users.name',
            'payment_types.Type')
            ->where($data[0][0],$data[0][1])
            ->where($data[1][0],$data[1][1])
            ->whereBetween('invoices.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('grns.Status',0)
            ->get();
            break;

            case 3: $grnGet = Grn::join('suppliers','suppliers.id','grns.Supplier_Id')
            ->join('stores','stores.id','grns.Stock_Id')
            ->join('users','users.id','grns.User_Id')
            ->join('payment_types','payment_types.id','grns.Payment_Type_Id')
            ->select('grns.id','suppliers.Name as SName','grns.Grn_No'
            ,'grns.InvoiceId','grns.Grn_Total',
            'grns.Discount','stores.Name','users.name',
            'payment_types.Type')
            ->where($data[0][0],$data[0][1])
            ->where($data[1][0],$data[1][1])
            ->where($data[2][0],$data[2][1])
            ->whereBetween('invoices.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('grns.Status',0)
            ->get();
            break;

            case 4: $grnGet = Grn::join('suppliers','suppliers.id','grns.Supplier_Id')
            ->join('stores','stores.id','grns.Stock_Id')
            ->join('users','users.id','grns.User_Id')
            ->join('payment_types','payment_types.id','grns.Payment_Type_Id')
            ->select('grns.id','suppliers.Name as SName','grns.Grn_No'
            ,'grns.InvoiceId','grns.Grn_Total',
            'grns.Discount','stores.Name','users.name',
            'payment_types.Type')
            ->where($data[0][0],$data[0][1])
            ->where($data[1][0],$data[1][1])
            ->where($data[2][0],$data[2][1])
            ->where($data[3][0],$data[3][1])
            ->whereBetween('invoices.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('grns.Status',0)
            ->get();
            break;

        }
     
        return json_encode($grnGet);
    }

  
    
    public static function getRecord($data)
    {
        $count = count($data) - 1;
        error_log($count); 
        $recordGet;
        switch($data[$count]-2){
            case 0:error_log('case oq1');
            $recordGet = RecordTransection::join('branches','branches.id','record_transections.Branch')
            ->join('users','users.id','record_transections.User')
            ->select('record_transections.id','branches.Name as BName','record_transections.Type','record_transections.Code','users.name','record_transections.created_at')
            ->whereBetween('record_transections.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('record_transections.Status',0)
            ->get();
            break;
            case 1: error_log("case 1"); 
            $recordGet = RecordTransection::join('branches','branches.id','record_transections.Branch')
            ->join('users','users.id','record_transections.User')
            ->select('record_transections.id','branches.Name as BName','record_transections.Type','record_transections.Code','users.name','record_transections.created_at')
            ->where($data[0][0],$data[0][1])
            ->whereBetween('record_transections.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('record_transections.Status',0)
            ->get();
            break;

            case 2: $recordGet = RecordTransection::join('branches','branches.id','record_transections.Branch')
            ->join('users','users.id','record_transections.User')
            ->select('record_transections.id','branches.Name as BName','record_transections.Type','record_transections.Code','users.name','record_transections.created_at')
            ->where($data[0][0],$data[0][1])
            ->where($data[1][0],$data[1][1])
            ->whereBetween('record_transections.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('record_transections.Status',0)
            ->get();
            break;

            case 3: $recordGet = RecordTransection::join('branches','branches.id','record_transections.Branch')
            ->join('users','users.id','record_transections.User')
            ->select('record_transections.id','branches.Name as BName','record_transections.Type','record_transections.Code','users.name','record_transections.created_at')
            ->where($data[0][0],$data[0][1])
            ->where($data[1][0],$data[1][1])
            ->where($data[2][0],$data[2][1])
            ->whereBetween('record_transections.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('record_transections.Status',0)
            ->get();
            break;

            case 4: $recordGet = RecordTransection::join('branches','branches.id','record_transections.Branch')
            ->join('users','users.id','record_transections.User')
            ->select('record_transections.id','branches.Name as BName','record_transections.Type','record_transections.Code','users.name','record_transections.created_at')
            ->where($data[0][0],$data[0][1])
            ->where($data[1][0],$data[1][1])
            ->where($data[2][0],$data[2][1])
            ->where($data[3][0],$data[3][1])
            ->whereBetween('record_transections.created_at',array($data[$count-2][1],$data[$count - 1][1]))
            ->where('record_transections.Status',0)
            ->get();
            break;

        }
     
        return json_encode($recordGet);
    }

    
}
