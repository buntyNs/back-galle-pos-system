<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Validator;

class TestController extends Controller
{
    public $successStatus = 200;
 
    public function __construct()
    {
        return $this->middleware('auth:api');
    }

    public function index()
    {
        //
        return "test";
    }

 
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        // $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->name;
        return response()->json(['success' => $success], $this->successStatus);
    }


    public function store(Request $request)
    {
        //
    }

 
    public function show(test $test)
    {
        //
    }

  
    public function edit(test $test)
    {
        //
    }

  
    public function update(Request $request, test $test)
    {
        //
    }


    public function destroy(test $test)
    {
        //
    }
}
