<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->string('Name', 45)->nullable();
			$table->string('Brand', 45)->nullable();
			$table->string('Qty', 45)->nullable();
			$table->float('SellingPrice', 10, 0)->nullable();
			$table->float('BuyingPrice', 10, 0)->nullable();
			$table->integer('ItemPerCase')->nullable();
			$table->integer('Status')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}
