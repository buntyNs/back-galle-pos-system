<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransferDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transfer_details', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->dateTime('Date')->nullable();
			$table->integer('Qty')->nullable();
			$table->integer('Transfer_Id')->index('fk_TransferDetail_Transfer1_idx');
			$table->integer('Item_Id')->index('fk_TransferDetail_Item1_idx');
			$table->float('Price', 10, 0)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transfer_details');
	}

}
