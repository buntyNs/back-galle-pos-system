<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoiceDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoice_data', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->integer('Qty')->nullable();
			$table->integer('FreeQty')->nullable();
			$table->integer('Discount')->nullable();
			$table->integer('SellingPrice')->nullable();
			$table->float('TotalPrice', 10, 0)->nullable();
			$table->integer('Invoice_Id')->index('fk_InvoiceData_Invoice_idx');
			$table->integer('Item_Id')->index('fk_InvoiceData_Item1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoice_data');
	}

}
