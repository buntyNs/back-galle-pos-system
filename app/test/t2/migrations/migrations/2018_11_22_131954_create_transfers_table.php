<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransfersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transfers', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->string('Date', 45)->nullable();
			$table->integer('Status')->nullable();
			$table->integer('userDetails_Id')->index('fk_Transfer_userDetails1_idx');
			$table->integer('From')->index('fk_Transfer_Store1_idx');
			$table->integer('To')->index('fk_Transfer_Store2_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transfers');
	}

}
